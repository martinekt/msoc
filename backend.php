<?php
    $q = $_REQUEST["q"];

    switch($q){
        case "D":   //Názvy souborů v adr.
            $name = $_REQUEST["na"];
            $files = scandir($name);
            foreach($files as $key => $i){
                if($i == "." || $i == "..")
                    continue;

                if($key != 2)
                    echo "|";

                echo $i;
            }
            break;
        case "F":   //Pošle celý kód
            $file = file($_REQUEST["na"] . "/" . $_REQUEST["nf"]);

            foreach($file as $i){
                echo "<li><pre>" . $i . "</pre></li>";
            }
            break;
        case "C":   //Pošle část kódu
            $file = file($_REQUEST["na"] . "/" . $_REQUEST["nf"]);

            $od = $_REQUEST["od"];
            $do = $_REQUEST["do"];

            if($od < 1)
                $od = 1;
            if($do > count($file))
                $do = count($file);

            for($i = $od; $i <= $do; $i++){
                echo "<li><pre>" . $file[$i - 1] . "</pre></li>";
            }
            break;
        case "I":   //Pošle info o kódu
            $file = file($_REQUEST["na"] . "/" . $_REQUEST["nf"]);

            $out = "";
            foreach($file as $line){
                if(preg_match("/import/", $line) == 1){
                    $out .= "<li>";
                    $line = str_replace("import", "", $line);
                    $line = str_replace(";", "", $line);
                    $out .= $line . "</li>";
                }
            }
            echo "<li>Packeges: <ul>$out</ul></li>";

            foreach($file as $line){
                if(preg_match("/class/", $line)){
                    $line = preg_replace("/public|protected|private|abstract|class|extends|implements|{/", "", $line);
                    $items = explode(" ", $line);
                    foreach($items as $item){
                        if(strcmp($item, "")){
                            $out = $item;
                            break;
                        }
                    }
                    break;
                }
            }
            echo "<li>Název třídy: $out</li>";

            $out = "neobsahuje";
            foreach($file as $line){
                if(preg_match("/public static void main/", $line)){
                    $out = "obsahuje";
                    break;
                }
            }
            echo "<li>Třída $out metodu main</li>";
            break;
        case "M":   //Pošle metody třídy
            $file = file($_REQUEST["na"] . "/" . $_REQUEST["nf"]);

            echo "<li>Statické:<ul>";
            foreach($file as $line){
                if(preg_match("/public|private|protected/", $line)){
                    if(preg_match("/class/", $line) || preg_match("/;/", $line) || !preg_match("/static/", $line))
                        continue;

                    $out = preg_filter("/public|protected|private|static|{|}/", "", $line);

                    echo "<li>$out <button onclick='vypsatTeloMetody(`$line`)'>Vypsat tělo metody</button></li>";
                }
            }
            echo "</ul></li><li>instanční:<ul>";
            foreach($file as $line){
                if(preg_match("/public|private|protected/", $line)){
                    if(preg_match("/class/", $line) || preg_match("/;/", $line) || preg_match("/static/", $line))
                        continue;

                    $out = preg_filter("/public|protected|private|{|}/", "", $line);

                    echo "<li>$out <button onclick='vypsatTeloMetody(`$line`)'>Vypsat tělo metody</button></li>";
                }
            }
            echo "</ul></li>";
            break;
        case "P":   //Pošle proměnné třídy
            $file = file($_REQUEST["na"] . "/" . $_REQUEST["nf"]);

            echo "<li>Statické:<ul>";
            foreach($file as $line){
                if(preg_match("/public|private|protected/", $line)){
                    if(preg_match("/class/", $line) || !preg_match("/;/", $line))
                        continue;

                    $out = preg_filter("/;/", "", $line);

                    echo "<li>$out</li>";
                }
            }
            echo "</ul></li>";
            break;
        case "V":   //Pošle tělo metody
            $file = file($_REQUEST["na"] . "/" . $_REQUEST["nf"]);

            $metoda = $_REQUEST["metoda"];

            $start = 0;
            $end = 0;

            foreach($file as $key => $line){
                if(!strcmp(trim($metoda), trim($line))){
                    $start = $key;
                    break;
                }
            }

            $bracket = 0;
            foreach($file as $key => $line){
                if($key < $start)
                    continue;

                if(preg_match("/{/", $line))
                    $bracket++;
                if(preg_match("/}/", $line))
                    $bracket--;

                if($key > $start && $bracket == 0){
                    $end = $key;
                    break;
                }
            }

            for($i = $start; $i <= $end; $i++){
                echo "<li><pre>" . $file[$i] . "</pre></li>";
            }

            break;
    }

    function getSoubor($nf, $na){
        return file($na . "/" . $nf);
    }
?>