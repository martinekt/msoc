import java.util.*;

public class math
{
    public double anyrt(int cislo, int exp){
        if(cislo < 0)
            throw new IllegalArgumentException("Čísla musí být kladná");
        double pomoc1 = 0;
        for(int j = 0; j < 15; j++){
            boolean mensi = true;
            double pomoc2 = 0;
            double desMist = 1 / power(10, j);
            while(mensi){
                pomoc1 += desMist;
                pomoc2 = power(pomoc1 ,exp);
                if(pomoc2 > cislo)
                    mensi = false;
            }
            pomoc1 -= desMist;
        }
        return pomoc1;
    }
    
    public double power(double cislo, int exp){
        boolean zap = false;
        if(exp < 0){
            zap = true;
            exp *= -1;
        }
        double a = 1;
        for(int i = 0; i < exp; i++){
            a *= cislo;
        }
        if(zap)
            a = 1 / a;
        return a;
    }
    
    public double PI(){
        double pi = 0;
        double predchozi = 1;
        for(int i = 9999999; i > 1; i -= 2){
            predchozi = 6 + (power(i, 2) / predchozi);
        }
        pi = 3 + (1 / predchozi);
        return pi;
    }
    
    public double E(){
        double e = 0;
        for(int i = 0; i < 25; i++){
            e += 1.0 / faktorial(i);
        }
        return e;
    }
    
    public long faktorial(long cislo){
        if(cislo > 25 || cislo < 0)
            throw new IllegalArgumentException("Číslo musí být v rozmezí 0 - 25");
        long a = 1;
        for(long i = cislo; i > 1; i--){
            a *= i;
        }
        return a;
    }
    
    public double sin(double deg){
        if(floor(deg / 180) % 2 == 1)
            deg *= -1;
        deg %= 180;
        double sinus = 0;
        double rad = deg / 180 * PI();
        boolean znaminko = true;
        
        for(int i = 1; i < 25; i += 2){
            if(znaminko)
                sinus += power(rad, i) / faktorial(i);
            else
                sinus -= power(rad, i) / faktorial(i);
            znaminko = !znaminko;
        }
                
        return round(sinus, 10);
    }
    
    public double cos(double deg){
        return sin(90 - deg);
    }
    
    public double tg(double deg){
        return sin(deg) / sin(90 - deg);
    }
    
    public double round(double cislo, int poc){
        double a = cislo;
        double modulo = 1.0 / power(10, poc);
        a = a % modulo;
        double vysledek = cislo - a;
        a = a * power(10, poc + 1);
        
        if(a >= 5){
            vysledek += modulo;    
        }
        
        return vysledek;
    }
    
    public int floor(double cislo){
        return (int)cislo;
    }
}
