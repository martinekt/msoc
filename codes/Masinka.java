public class Masinka
{
    private Obdelnik komin;
    private Obdelnik telo;
    private Elipsa[] kolo;
    
    public static int cas = 5;
    
    public Masinka(){
        telo = new Obdelnik(0, 110, 80, 40, Barva.MODRA);
        komin = new Obdelnik(20, 85, 15, 25, Barva.SEDA);
        kolo = new Elipsa[2];
        kolo[0] = new Elipsa(0, 150, 25, 25, Barva.CERNA);
        kolo[1] = new Elipsa(55, 150, 25, 25, Barva.CERNA);
    }
    
    public Masinka(int x, int y){
        telo = new Obdelnik(x, y + 25, 80, 40, Barva.MODRA);
        komin = new Obdelnik(x + 20, y, 15, 25, Barva.SEDA);
        kolo = new Elipsa[2];
        kolo[0] = new Elipsa(x, y + 65, 25, 25, Barva.CERNA);
        kolo[1] = new Elipsa(x + 55, y + 65, 25, 25, Barva.CERNA);
    }
    
    public void pohybVpravo(){
        do{
            telo.posunVpravo(1);
            komin.posunVpravo(1);
            kolo[0].posunVpravo(1);
            kolo[1].posunVpravo(1);
            cekej(20);
        }while(telo.getX() <= Platno.SIRKA_0);
    }
    
    public void pohybDolu(){
        telo.setRozmer(telo.getVyska(), telo.getSirka());
        telo.posunVpravo(25);
        komin.setRozmer(komin.getVyska(), komin.getSirka());
        komin.posunVpravo(45);
        komin.posunDolu(45);
        kolo[0].posunDolu(-40);
        kolo[1].posunVpravo(-55);
        kolo[1].posunDolu(15);
        telo.nakresli();
        do{
            telo.posunDolu(cas);
            komin.posunDolu(cas);
            kolo[0].posunDolu(cas);
            kolo[1].posunDolu(cas);
            cekej(20);
        }while(telo.getY() <= Platno.SIRKA_0);
    }
    
    public void pohybPoObvodu(int poc){
        telo.setRozmer(telo.getVyska(), telo.getSirka());
        telo.posunVpravo(25);
        komin.setRozmer(komin.getVyska(), komin.getSirka());
        komin.posunVpravo(45);
        komin.posunDolu(45);
        kolo[0].posunDolu(-40);
        kolo[1].posunVpravo(-55);
        kolo[1].posunDolu(15);
        telo.nakresli();
        do{
            telo.posunDolu(1);
            komin.posunDolu(1);
            kolo[0].posunDolu(1);
            kolo[1].posunDolu(1);
            cekej(cas);
        }while(telo.getY() <= Platno.SIRKA_0 - 80);
        
        for(int i = poc; i > 0; i--){
            telo.setRozmer(telo.getVyska(), telo.getSirka());
            telo.posunVpravo(-25);
            telo.posunDolu(15);
            komin.setRozmer(komin.getVyska(), komin.getSirka());
            komin.posunVpravo(-45);
            komin.posunDolu(-30);
            kolo[0].posunDolu(55);
            kolo[1].posunVpravo(55);
            telo.nakresli();
            kolo[0].nakresli();
            komin.nakresli();
            do{
                telo.posunVpravo(1);
                komin.posunVpravo(1);
                kolo[0].posunVpravo(1);
                kolo[1].posunVpravo(1);
                cekej(cas);
            }while(telo.getX() <= Platno.SIRKA_0 - 80);
            telo.setRozmer(telo.getVyska(), telo.getSirka());
            telo.posunVpravo(15);
            telo.posunDolu(-15);
            komin.setRozmer(komin.getVyska(), komin.getSirka());
            komin.posunVpravo(-30);
            komin.posunDolu(60);
            kolo[0].posunVpravo(55);
            kolo[1].posunDolu(-55);
            telo.nakresli();
            kolo[0].nakresli();
            komin.nakresli();
            do{
                telo.posunDolu(-1);
                komin.posunDolu(-1);
                kolo[0].posunDolu(-1);
                kolo[1].posunDolu(-1);
                cekej(cas);
            }while(telo.getY() >= 0);
            telo.setRozmer(telo.getVyska(), telo.getSirka());
            telo.posunVpravo(-15);
            telo.posunDolu(25);
            komin.setRozmer(komin.getVyska(), komin.getSirka());
            komin.posunVpravo(55);
            komin.posunDolu(15);
            kolo[0].posunDolu(-55);
            kolo[1].posunVpravo(-55);
            telo.nakresli();
            kolo[0].nakresli();
            komin.nakresli();
            do{
                telo.posunVpravo(-1);
                komin.posunVpravo(-1);
                kolo[0].posunVpravo(-1);
                kolo[1].posunVpravo(-1);
                cekej(cas);
            }while(telo.getX() >= 0);
            telo.setRozmer(telo.getVyska(), telo.getSirka());
            telo.posunVpravo(25);
            telo.posunDolu(-25);
            komin.setRozmer(komin.getVyska(), komin.getSirka());
            komin.posunVpravo(20);
            komin.posunDolu(-45);
            kolo[0].posunVpravo(-55);
            kolo[1].posunDolu(55);
            telo.nakresli();
            kolo[0].nakresli();
            komin.nakresli();
            if(i == 1){
                    do{
                    telo.posunDolu(1);
                    komin.posunDolu(1);
                    kolo[0].posunDolu(1);
                    kolo[1].posunDolu(1);
                    cekej(cas);
                }while(telo.getY() <= Platno.SIRKA_0);
            }
            else{
                    do{
                    telo.posunDolu(1);
                    komin.posunDolu(1);
                    kolo[0].posunDolu(1);
                    kolo[1].posunDolu(1);
                    cekej(cas);
                }while(telo.getY() <= Platno.SIRKA_0 - 80);
            }
        }
    }
    
     private static void cekej( int milisekund )
    {
        try {
            Thread.sleep( milisekund);
        }catch( InterruptedException e) {
            throw new RuntimeException( "Cekani bylo preruseno", e );
        }
    }
}
