package vykon;

import java.util.*;

public class Vykon
{
    public static Scanner sc = new Scanner(System.in);
    
    public static void pocitej(double U, double I, double F){
        if(F < -90 || F > 90)
            throw new IllegalArgumentException("F musí být v intervalu <-90, 90>");
        System.out.println("Výkon = " + (U * I * Math.cos(Math.toRadians(F))) + " W");
    }
    
    public static void main(String[] args){
        try{
            System.out.print("Zadejte U: ");
            double U = sc.nextInt();
            System.out.print("Zadejte I: ");
            double I = sc.nextInt();
            System.out.print("Zadejte φ: ");
            double F = sc.nextInt();
            pocitej(U, I, F);
        }
        catch (InputMismatchException e){
            System.out.println("Nebylo zadáno číslo");
        }
        catch (IllegalArgumentException e){
            System.out.println("φ není z intervalu\n" + e.getMessage());
        }
    }
}
