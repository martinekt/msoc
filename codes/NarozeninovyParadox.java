import java.util.Scanner;

public class NarozeninovyParadox {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Zadejte počet lidí: ");
        int n = sc.nextInt();

        double x = 1;
        for(int i = 1; i < n; i++){
            x *= ((365 - i) / 365.0);
        }
        x = 1 - x;
        System.out.println("Šance: " + x);
    }
}
