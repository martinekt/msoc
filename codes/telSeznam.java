import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class telSeznam {
    private JPanel panel;
    private JLabel nadpis;
    private JComboBox comboBox1;
    private JLabel vypisAdresa;
    private JTextField jmenoTextField;
    private JTextField prijmeniTextField;
    private JTextField telCisloTextField;
    private JTextField emailTextField;
    private JTextField adresaTextField;
    private JTextField nazevSouboruTextField;
    private JButton nacistButton;
    private JButton ulozitButton;
    private JLabel Error;
    private JLabel vypisTelefon;
    private JLabel vypisEmail;
    private JButton pridatButton;
    private JButton smazatButton;

    private ArrayList<zaznam> zaznamy = new ArrayList<zaznam>();

    public void vypisComboBox(){
        comboBox1.removeAllItems();
        for(int i = 0; i < zaznamy.size(); i++){
            String jmeno = zaznamy.get(i).getJmeno() + " " + zaznamy.get(i).getPrijmeni();
            comboBox1.addItem(jmeno);
        }
    }

    public void vypisHodnot(){
        int misto = comboBox1.getSelectedIndex();
        if(misto == -1){
            vypisAdresa.setText("");
            vypisTelefon.setText("");
            vypisEmail.setText("");
            return;
        }
        vypisAdresa.setText("Adresa: " + zaznamy.get(misto).getAdresa());
        vypisTelefon.setText("Telefon: " + zaznamy.get(misto).getTelefon());
        vypisEmail.setText("Email: " + zaznamy.get(misto).getEmail());
    }

    public void pridatZaznam(){
        zaznamy.add(new zaznam(jmenoTextField.getText(), prijmeniTextField.getText(), adresaTextField.getText(), telCisloTextField.getText(), emailTextField.getText()));
        vypisComboBox();
        jmenoTextField.setText("");
        prijmeniTextField.setText("");
        adresaTextField.setText("");
        telCisloTextField.setText("");
        emailTextField.setText("");
    }

    public void smazatHodnotu(){
        int misto = comboBox1.getSelectedIndex();
        if(misto == -1)
            return;
        zaznamy.remove(comboBox1.getSelectedIndex());
        vypisComboBox();
    }

    public telSeznam() {
        nacistButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(zapisNacteni.nacistZeSouboru(nazevSouboruTextField.getText(), zaznamy)) {
                    Error.setText("");
                    vypisComboBox();
                }
                else{
                    Error.setForeground(Color.RED);
                    Error.setText("Soubor neexistuje");
                }
            }
        });
        comboBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                vypisHodnot();
            }
        });
        pridatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pridatZaznam();
            }
        });
        ulozitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(zapisNacteni.ulozitSoubor(nazevSouboruTextField.getText(), zaznamy)){
                    Error.setForeground(Color.GREEN);
                    Error.setText("Zápis byl úspěšný!");
                }
                else{
                    Error.setForeground(Color.RED);
                    Error.setText("Zápis neproběhl správně");
                }
            }
        });
        smazatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                smazatHodnotu();
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("telSeznam");
        frame.setContentPane(new telSeznam().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
    }
}
