

public abstract class Obrazec
{
    private int x, y;
    public Obrazec(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public abstract double obvod();
    public abstract double obsah();
}
