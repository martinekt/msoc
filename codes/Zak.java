public class Zak extends Osoba
{
    private String skola;
    private int rocnik;
    
    public Zak(String jmeno, String rodne_cislo, String skola, int rocnik)
    {
        super(jmeno, rodne_cislo);
        this.skola = skola;
        this.rocnik = rocnik;
    }
}

